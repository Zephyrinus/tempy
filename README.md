---
title: 'Project Tempy'
subtitle: 'Tempy is an environmental robot built to explore and measure potentially dangerous environments, collect data, and send them to headquorters'
authors: ['Romulus Prundis \<prundisv@gmail.com\>']
date: \today
left-header: \today
right-header: Project plan
skip-toc: false
skip-tof: true
---


# Project Tempy
This is a school project that was developed during the first  semester (2021) at UCL, the topic is using both embedded, programming and electronic skills to built a rover that can measure dangerouns environments and send the data to the user.
This project was developed as a prototype more than a fully end product. In addition it includes also some project management file such as Moscow, FURPS and Uml diagram to understand how to code is working.
The robot is controlled using a playstation controller.
The rover has a bme 280 sensor that allows to measured data like temperature, pressure, humidity and altitude.
The programming language used is Python.


# Part of this team are 

• Simon Christiansen - @V1ncit 

 • Simon Slamka - @simtoon1011

 • Romulus V. Prundis - @Zephyrinus 

 • Bogdan Butenche - @bogdan7978

 



# Components for the robot

- [x] 4 motors and 4 wheels
- [X] PWM 2CH motorcontroller (L298N)
- [x] Resistors
- [x] LEDs
- [x] IR sensors
- [x] Temp/humidity sensors (bme280)
- [x] RPi (Raspberry Pi zero)
- [x] Electronic buzzer alarm
- [x] The rover skeleton (chassis)
- [x] Playstation controller



